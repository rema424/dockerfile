# dockerfile
## Go
### In Short
bash
```
$ git clone https://gitlab.com/rema424/dockerfile.git
$ cd dockerfile
$ docker-compose build go
$ docker-compose run go dep init

...coding

$ docker-compose run go dep ensure
$ docker-compose run go go run main.go
$ docker-compose run gin -p 8080
```

### For More Information
Dockerfile
```docker:Dockerfile
FROM golang
```

docker-compose.yml
```yml:docker-compose.yml
version: '3'
services:
  go:
    build: ./go
    tty: true
    ports:
      - 8080:8080
```

bash
```bash
docker-compose build go
docker-compose up -d go
docker-compose ps

docker-compose exec go bash

go env

pwd
echo $GOPATH
ls
ls $GOPATH

echo $PATH

exit

docker cp `docker-compose ps -q go`:/go/src ./go/src
docker cp `docker-compose ps -q go`:/go/bin ./go/bin

docker-compose down
```

docker-compose.yml
```diff:docker-compose.yml
  version: '3'
  services:
    go:
      build: ./go
+     volumes:
+       - ./go:/go
      tty: true
      ports:
        - 8080:8080
```

bash
```bash
docker-compose up -d go
docker-compose exec go bash

mkdir -p $GOPATH/src/app && cd $GOPATH/src/app
dep init
touch main.go

exit

docker-compose down
```

main.go
```go:main.go
package main

import (
  "os"
  "github.com/gin-gonic/gin"
)

func main() {
  r := gin.Default()

  r.GET("/ping", func(c *gin.Context) {
    c.JSON(200, gin.H{
      "message": "pong",
    })
  })

  port := os.Getenv("PORT")
  if len(port) == 0 {
    port = "8080"
  }

  r.Run(":" + port)	// listen and serve on 0.0.0.0:8080 
}
```

docker-compose.yml
```diff:docker-compose.yml
  version: '3'
  services:
    go:
      build: ./go
+     working_dir: /go/src/app
+     command: gin -p 8080
      volumes:
        - ./go:/go
      tty: true
      ports:
        - 8080:8080
```

Dockerfile
```diff:Dockerfile
  FROM golang

+ ENV GOBIN /go/bin

+ ADD . /go
+ WORKDIR /go/src/app

+ RUN dep ensure

+ EXPOSE 8080
+ CMD ["gin","-p","8080"]
```

```bash
docker-compose up --build go

curl -Ss -i localhost:8080/ping; echo

HTTP/1.1 200 OK
Content-Length: 18
Content-Type: application/json; charset=utf-8
Date: Sat, 21 Jul 2018 05:51:15 GMT

{"message":"pong"}
```
